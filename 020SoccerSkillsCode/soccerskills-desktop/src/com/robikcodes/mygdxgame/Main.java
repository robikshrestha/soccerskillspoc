package com.robikcodes.mygdxgame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.robikcodes.mygdxgame.tries.Man3D;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "soccerskills";
		cfg.useGL20 = false;
		cfg.width = 480;
		cfg.height = 320;
		
		//new LwjglApplication(new MyGdxGame(), cfg);
		new LwjglApplication(new Man3D(), cfg);
	}
}
