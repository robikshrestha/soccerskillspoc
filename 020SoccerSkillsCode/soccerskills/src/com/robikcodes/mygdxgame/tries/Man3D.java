package com.robikcodes.mygdxgame.tries;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.utils.Array;

public class Man3D implements ApplicationListener {

	protected AssetManager assetManager;
	protected PerspectiveCamera cam;
	protected ModelBatch modelBatch;
	protected Array<Model> modelArray;
	protected Array<ModelInstance> modelInstanceArray;
	protected Environment env;
	protected CameraInputController camController;
	protected boolean modelsReady = false;
	@Override
	public void create() {
		createCam();
		modelBatch = new ModelBatch();
		assetManager = new AssetManager();
		modelArray = new Array<Model>();
		modelInstanceArray = new Array<ModelInstance>();
		startLoadingAssets();
	}

	//Create new camera
	protected void createCam() {
		cam = new PerspectiveCamera();
		cam.viewportWidth = Gdx.graphics.getWidth();
		cam.viewportHeight = Gdx.graphics.getHeight();
		cam.near = 0.1f;
		cam.far = 300f;
		cam.fieldOfView = 67;
		cam.position.set(10f, 10f, 10f);
		cam.lookAt(0, 0, 0);
		cam.update();
		camController = new CameraInputController(cam);
		Gdx.input.setInputProcessor(camController);
	}

	//Register all assets with assetManager
	protected void startLoadingAssets(){
		assetManager.load("data/models/newMan.g3db", Model.class);
	}
	
	/**
	 * Once the assetManager is updated properly, models can be initialized
	 */
	protected void initModels(){
		Model humanModel = (Model)assetManager.get("data/models/newMan.g3db");
		modelArray.add(humanModel);
		modelInstanceArray.add(new ModelInstance(humanModel));
		modelsReady = true;
	}
	
	protected void createEnv(){
		env = new Environment();
		env.set(new ColorAttribute(ColorAttribute.AmbientLight,0.5f,1f,0f,1f));
		env.add(new DirectionalLight().set(1f, 0.5f, 0.1f, 0f, 1f, 0.5f));
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	
	@Override
	public void render() {
		camController.update();
		//If assetManager has completed loading, then initialize models
		if(!modelsReady && assetManager.update()){
			initModels();
		}
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(Gdx.gl20.GL_COLOR_BUFFER_BIT | Gdx.gl20.GL_DEPTH_BUFFER_BIT);
		if(modelInstanceArray!=null){
			modelBatch.begin(cam);
			modelBatch.render(modelInstanceArray, env);
			modelBatch.end();
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		modelBatch.dispose();
		modelInstanceArray.clear();
		assetManager.dispose();
	}

}
